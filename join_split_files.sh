#!/bin/bash

cat system/system/app/webview/webview.apk.* 2>/dev/null >> system/system/app/webview/webview.apk
rm -f system/system/app/webview/webview.apk.* 2>/dev/null
cat system/system/priv-app/MicroGCore/MicroGCore.apk.* 2>/dev/null >> system/system/priv-app/MicroGCore/MicroGCore.apk
rm -f system/system/priv-app/MicroGCore/MicroGCore.apk.* 2>/dev/null
cat system/system/system_ext/priv-app/PrimeBrowser/PrimeBrowser.apk.* 2>/dev/null >> system/system/system_ext/priv-app/PrimeBrowser/PrimeBrowser.apk
rm -f system/system/system_ext/priv-app/PrimeBrowser/PrimeBrowser.apk.* 2>/dev/null
