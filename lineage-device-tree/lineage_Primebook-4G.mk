#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from Primebook-4G device
$(call inherit-product, device/floydwiz_technologies/Primebook-4G/device.mk)

PRODUCT_DEVICE := Primebook-4G
PRODUCT_NAME := lineage_Primebook-4G
PRODUCT_BRAND := Floydwiz
PRODUCT_MODEL := Primebook-4G
PRODUCT_MANUFACTURER := floydwiz_technologies

PRODUCT_GMS_CLIENTID_BASE := android-floydwiz_technologies

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="Primebook-4G-user 11 001.01.00.14 mp1V8382 release-keys"

BUILD_FINGERPRINT := Floydwiz/Primebook-4G/Primebook-4G:11/001.01.00.14/Primebook-4G_Primebook-4G_001.01.00.14_230223:user/release-keys
