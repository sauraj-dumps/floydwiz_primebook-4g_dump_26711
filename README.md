## Primebook-4G-user 11 001.01.00.14 mp1V8382 release-keys
- Manufacturer: floydwiz-technologies
- Platform: mt6771
- Codename: Primebook-4G
- Brand: Floydwiz
- Flavor: Primebook-4G-user
- Release Version: 11
- Kernel Version: 4.14.186
- Id: 001.01.00.14
001.01.00.14
- Incremental: Primebook-4G_Primebook-4G_001.01.00.14_230223
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Floydwiz/Primebook-4G/Primebook-4G:11/001.01.00.14/Primebook-4G_Primebook-4G_001.01.00.14_230223:user/release-keys
- OTA version: 
- Branch: Primebook-4G-user-11-001.01.00.14-mp1V8382-release-keys
- Repo: floydwiz_primebook-4g_dump_26711
